package DB;
 

	import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.PreparedStatement;
	import java.sql.SQLException;

	//import java.sql.*;
	public class InsertInut {

		private static InsertInut jb;

		private String[][] m;
		 

		private InsertInut() {
		}

		public static InsertInut getInstance() {
			if (jb == null) {
				jb = new InsertInut();
			}
			return jb;
		}

		// MySQL Connection

		public Connection getConnection() throws ClassNotFoundException, SQLException {
			Connection con = null;
		
			
		// Check ip address
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/csd13","root","root");
			return con;
		}

		public void insertIntoTable(int id, String name, int marks) throws SQLException {
			Connection con = null;
			PreparedStatement ps = null;
			m = new String[6][3];
			try {
				for (int i = 0; i < m.length; i++) {

					con = getConnection();
					ps = con.prepareStatement("INSERT INTO student(id,name,marks)VALUES(?,?,?)");
					ps.setInt(1, id);
					ps.setString(2, name);
					ps.setInt(3, marks);
					ps.executeUpdate();
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (ps != null) {
					ps.close();
				}
				if (con != null) {
					con.close();
				}
			}
		}

		public void createTable(String tableName) throws SQLException {

			Connection con = null;
			PreparedStatement ps = null;
			try {
				con = getConnection();
				String statement = "CREATE TABLE " + tableName + "(id INT(5), name VARCHAR(50), marks INT(2))";
				// System.out.println(statement);
				ps = con.prepareStatement(statement);
				ps.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (ps != null) {
					try {
						ps.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}

			}
		}
	}



