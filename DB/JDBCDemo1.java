package DB;

import java.sql.Connection;
import java.sql.SQLException;

public class JDBCDemo1 {

	public static void main(String[] args) {
		JDBC1 jdbc = JDBC1.getInstance();
		
		try {
			Connection con = jdbc.getConnection();
			System.out.println("************Connected Successfully*******");
			System.out.println(con);
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (SQLException e) {
			e.printStackTrace();	
		}
	}

}
