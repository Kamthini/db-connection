package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JDBC1 {
	
	private static JDBC1 jb1;
	
	private JDBC1() {}
	
	public static JDBC1 getInstance() {
		if (jb1 == null) {
			jb1 = new JDBC1();
		} return jb1;
	}
	
	public Connection getConnection() throws ClassNotFoundException, SQLException{
		Connection con =null;
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/csd13","root","root");
		
		return con;
		
	}

	public void insertIntoTable(String name,String pass) throws SQLException{
		Connection con=null;
		PreparedStatement ps=null;
		
	try {
		con=getConnection();
		ps=con.prepareStatement("INSERT INTO userdata(name,password) VALUES (kani,123)");
		ps.setString(1, name);
		ps.setString(2, pass);
		ps.executeUpdate("kani,123");	
	}
	catch(Exception e) {
		e.printStackTrace();
	}
	finally{
		if (ps!=null) {
			ps.close();
		}
		if (con!=null) {
			con.close();
		}
	}
	}
 
	 

}
